-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: golang-github-libdns-libdns
Binary: golang-github-libdns-libdns-dev
Architecture: all
Version: 0.2.0-1
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Ganesh Pawar <pawarg256@gmail.com>
Homepage: https://github.com/libdns/libdns
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-libdns-libdns
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-libdns-libdns.git
Testsuite: autopkgtest-pkg-go
Build-Depends: debhelper-compat (= 12), dh-golang, golang-any
Package-List:
 golang-github-libdns-libdns-dev deb devel optional arch=all
Checksums-Sha1:
 bfc626ac2bb930ffbdcb17128125231372409515 5805 golang-github-libdns-libdns_0.2.0.orig.tar.gz
 ac721eb01997d2c095b9d5cddfc05dae4e6fbd16 2584 golang-github-libdns-libdns_0.2.0-1.debian.tar.xz
Checksums-Sha256:
 b4363f4aca4e8633f474480669d4924909f3856411ea4974e09481eca2a9fbfe 5805 golang-github-libdns-libdns_0.2.0.orig.tar.gz
 4ae9dc5f26f9c81cdba1f434b15462d648898c03ad10d956f7d9b427ec9756f8 2584 golang-github-libdns-libdns_0.2.0-1.debian.tar.xz
Files:
 7c569ab568d666b898492899f785315a 5805 golang-github-libdns-libdns_0.2.0.orig.tar.gz
 a5291ca0a480d108a53db30ffc5424d1 2584 golang-github-libdns-libdns_0.2.0-1.debian.tar.xz
Go-Import-Path: github.com/libdns/libdns

-----BEGIN PGP SIGNATURE-----

iQHIBAEBCgAyFiEELHKgyW/EWQAYj1bsbCD1Rh4I/UsFAmBXCZMUHHBhd2FyZzI1
NkBnbWFpbC5jb20ACgkQbCD1Rh4I/Uul6QwAn92029GP/IlbO/ilP/2R+k7kkIK+
kSf6C7Vl60yxAlfPGsqh26+zRQLPJR3+uqLJOzHtDpyjRRew+7QgmtLDq7NiJC53
BYkkOVCw97QJZSofrOZIgym+/98NPZYmu9Y1U5BM8928D25F8NBMxULOE8PWlrdJ
Rcv4vYcvWnyi7KxsTn07HjK3IryvyITOdNtgQgMRWRxgo9w0SfM1J1laHh25yxTO
mZf/IBE+4u4GSoFqTUiZb7CNf6GeF/r3KHa+TK59cDnun6sxcb1zvRdJabVRyt2x
037AttEHNzKk4LUJH2JYWfQI1G+G4PaAiwDCkvpMRTdjNtILzcgOCrtmvjbpMYSg
0PnexSUT6XOuNdrodn097NzTIKj+iH1QrnD4tl8p+inZkiLffehfCzvQDmCsfij0
gzdSgfYKyxZIzIkbvdYQU3DLEG084OuT4Tr3/AcYNoKSnKnVl3tOc5EVGzHBqest
2sMlxaQ8DhwB21LlQV4D5PoVfY5Krc2JZHSO
=RUex
-----END PGP SIGNATURE-----
